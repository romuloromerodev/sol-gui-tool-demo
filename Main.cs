﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SOL_GUI_Tool.UI;

using SOL_GUI_Tool.Managers;

using System.Collections.Generic;
using System.Linq;
using static SOL_GUI_Tool.UI.UIObject;
using static SOL_GUI_Tool.UI.SimpleSprite;
using static SOL_GUI_Tool.UI.TextBox;
using System;
using Windows.Storage.Pickers;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Storage;
using System.IO;
using Windows.UI.Popups;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Xml;
using System.Diagnostics;
using static SOL_GUI_Tool.UI.MouseEventsSystem;
using System.Reflection;
using System.Dynamic;
using Windows.Security.Cryptography;
using static SOL_GUI_Tool.UI.MouseEventsSystem.MouseEvents;
using System.Collections;
using Jint;
using Microsoft.Xna.Framework.Content;
using MonoGame.Extended.ViewportAdapters;
using System.Globalization;
using Jint.Runtime.Interop;
using Esprima;
using Newtonsoft.Json;
using Jint.Native.Object;
using Jint.Native;

using System.Text.RegularExpressions;
using static SOL_GUI_Tool.UI.ReflectionInfo;

namespace SOL_GUI_Tool
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Main : Game
    {
        public static Main MainInstance;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        List<UIObject> guiList;

        // Save and Load elements
        ComboBox fileMenu;

        // Actual window elements
        Frame mainWindowFrame;
        Label mainFrameCaption;

        // Toolbox elements
        Frame UIToolShelf;
        TextBox toolShelfTextbox;
        Label toolShelfLabel;
        Button toolShelfButton;
        CheckBox toolShelfCheckbox;
        ComboBox toolShelfComboBox;
        ToggleButton toolShelfToggle;
        SliderBar toolShelfSlider;
        MultiLineTextBox toolShelfMultilineTextbox;
        // Temporary solution
        // Properties Panel elements
        Frame itemPropertiesPanel;
        TextBox objPropText;
        Label objPropName;
      
        TextBox objPropTextureName;
        Button objPropTextureLoad;
        ComboBox objPropColor;
        ComboBox objPropTextColor;
        CheckBox objPropShowHide;
        TextBox objPropSearch;
        CheckBox objPropLock;
        ModifiableCombinedArrows objPropScaleX;
        ModifiableCombinedArrows objPropScaleY;
        ModifiableCombinedArrows objPropPositionX;
        ModifiableCombinedArrows objPropPositionY;
        ModifiableCombinedArrows objPropAlpha;
        ElementSelection objPropItemSelection;
        ComboBox objPropEvents;
        TextBoxConfirmPicker objPropEvtOnClickPicker;
        TextBoxConfirmPicker objPropEvtOnMouseOverPicker;
        TextBoxConfirmPicker objPropEvtOnMouseOutPicker;
        TextBoxConfirmPicker objPropComboAddItem;
        List<string> searchedElements;

        UIObject currentObject;
        public static ContentManager mainContentMgr;
        public static SpriteBatch mainBatch;
        public static Rectangle WndBounds { get; private set; }

        private ViewportAdapter adapter;

        private static Matrix matrix;
        public static Matrix Matrix
        {
            get { return matrix; }
            private set { matrix = value; }
        }

        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            mainContentMgr = Content;
            guiList = new List<UIObject>();
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        /// 

        public void Button1OnClick(object sender, MouseClickArgs args)
        {          
            Button btn = new Button();
            btn.TextColor.Color = Color.Black;
            btn.Editable = true;
            btn.Name = "Button2";
            btn.Active = true;
            btn.Alpha = 0.5f;            
          
            mainWindowFrame.AddItemSlot(new Point(300, 300), ElementType.BUTTON);
            mainWindowFrame.AddItem(btn, Point.Zero, ElementType.BUTTON);
            mainWindowFrame.Setup();
            MessageDialog msg = new MessageDialog("Item successfully created.");
            msg.ShowAsync();
        }
        public void Button1OnMouseOver(object sender, MouseOverArgs args)
        {
            Button btn = new Button();
            btn.TextColor.Color = Color.Black;
            btn.Editable = true;
            btn.Name = "Button2";
            btn.Active = true;
            btn.Alpha = 0.5f;

            mainWindowFrame.AddItemSlot(new Point(300, 300), ElementType.BUTTON);
            mainWindowFrame.AddItem(btn, Point.Zero, ElementType.BUTTON);
            mainWindowFrame.Setup();
            //MessageDialog msg = new MessageDialog("Item successfully created.");
            //msg.ShowAsync();
        }
        public void Button1OnMouseOut(object sender, MouseOutArgs args)
        {
            Button btn = new Button();
            btn.TextColor.Color = Color.Black;
            btn.Editable = true;
            btn.Name = "Button2";
            btn.Active = true;
            btn.Alpha = 0.5f;

            mainWindowFrame.AddItemSlot(new Point(300, 300), ElementType.BUTTON);
            mainWindowFrame.AddItem(btn, Point.Zero, ElementType.BUTTON);
            mainWindowFrame.Setup();
            MessageDialog msg = new MessageDialog("Item successfully created.");
            msg.ShowAsync();
        }
        public async Task SaveObjectToXml(List<UIObject> objectToSave, string filename)
        {
            //// stores an object in XML format in file called 'filename'
            var serializer = new DataContractSerializer(typeof(List<UIObject>));
            var settings = new XmlWriterSettings { Indent = true };
            var savePicker = new FileSavePicker();
            savePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;

            savePicker.FileTypeChoices.Add("XML", new List<string>() { ".xml" });
            savePicker.SuggestedFileName = "GUILayout";

            StorageFile file = await savePicker.PickSaveFileAsync();
            if (file != null)
            {
                // Prevent updates to the remote version of the file until
                // we finish making changes and call CompleteUpdatesAsync.
                CachedFileManager.DeferUpdates(file);             
                using (Stream stream = await file.OpenStreamForWriteAsync().ConfigureAwait(false))
                {
                    using (var writer = XmlWriter.Create(stream, settings))
                    {
                        serializer.WriteObject(writer, objectToSave);
                    }

                }
            }
        }

        public async static Task<List<UIObject>> ReadObjectFromXmlFileAsync()
        {
            List<UIObject> objectFromXml = default(List<UIObject>);            
            DataContractSerializer serializer = new DataContractSerializer(typeof(List<UIObject>));

            // Deserialize the data and read it from the instance.
            FileOpenPicker loadFilePicker = new FileOpenPicker();
            loadFilePicker.ViewMode = PickerViewMode.Thumbnail;
            loadFilePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            loadFilePicker.FileTypeFilter.Add(".xml");

            StorageFile file = await loadFilePicker.PickSingleFileAsync();
            if (file != null)
            {
                Stream stream = await file.OpenStreamForReadAsync().ConfigureAwait(false);
                XmlDictionaryReader reader =
                XmlDictionaryReader.CreateTextReader(stream, new XmlDictionaryReaderQuotas());

                objectFromXml = (List<UIObject>)serializer.ReadObject(reader, true);
                stream.Dispose();
            }

            return objectFromXml;

        }

        protected override void Initialize()
        {
            MainInstance = this;
            adapter = new DefaultViewportAdapter(GraphicsDevice);

            // Save and Load elements
            fileMenu = new ComboBox();
            fileMenu.DefaultOffset = new Point(4, 43);
            fileMenu.Position = new Point(32,32);
           
            //fileMenu.AddItemDefault();
           
            fileMenu.AddName("File", Color.Black, Singleton.Font.FONT(FontMgr.FontType.ARIAL));          
       

            fileMenu.AddNewItem("Save", () =>
            {
            });
            fileMenu.AddNewItem("Load", async () =>
            {               

                List<UIObject> elements = await ReadObjectFromXmlFileAsync();

                if (elements != null)
                {
                    for (int i = 0; i < elements.Count; i++)
                    {
                        UIObject uiElement = elements[i];

                        uiElement.AddTexturesPack();
                        uiElement.AddFontPack();

                        mainWindowFrame.AddItemSlot(uiElement.Position - mainWindowFrame.Position, uiElement.Type, uiElement.Priority);
                        mainWindowFrame.AddItem(uiElement, Point.Zero, uiElement.Type);
                        uiElement.AddSpriteRenderer(spriteBatch);
                        uiElement.AddStringRenderer(spriteBatch);

                        currentObject = uiElement;
                        currentObject.Editable = true;
                    }

              
                    mainWindowFrame.Setup();                                      
                    
                }
                fileMenu.Hide();

              
            });


            fileMenu.AddNewItem("Export", async () =>
            {
                string layoutName = "GUILayout";
                List<UIObject> uiElements = new List<UIObject>();
               
                for (int i = 0; i < mainWindowFrame.Slots.Count; i++)
                {
                    UIObject element = mainWindowFrame.Slots.ElementAt(i).Item;
                    uiElements.Add(element);
                }
              
                await SaveObjectToXml(uiElements, (layoutName) + ".xml");
                     
                fileMenu.Hide();
            });
            fileMenu.AddNewItem("Close", () =>
            {
                Exit();
            });
            //fileMenu.AddClickableDelegate();
            fileMenu.Editable = false;
            
            


            // Toolbox elements
            UIToolShelf = new Frame("UItoolboxDefaultTextureName", ElementDragOption.STATIC, SpritePriority.HIGH);
            UIToolShelf.Position = new Point(32, 100);

            toolShelfComboBox = new ComboBox();
            toolShelfComboBox.Editable = true;
            toolShelfComboBox.DefaultOffset = new Point(8, 43);
            toolShelfComboBox.AddItemDefault();
         
            toolShelfCheckbox = new CheckBox();
            toolShelfCheckbox.Editable = true;
            
            toolShelfLabel = new Label();
            toolShelfLabel.TextColor.Color = Color.DarkGray;
            toolShelfLabel.Editable = true;

            toolShelfTextbox = new TextBox();
            toolShelfTextbox.SetSelected(false);
            toolShelfTextbox.Editable = true;

            toolShelfButton = new Button();
            toolShelfButton.Editable = true;
            toolShelfButton.TextColor.Color = Color.Black;

            toolShelfToggle = new ToggleButton();
            toolShelfToggle.Editable = true;
            toolShelfToggle.Toggle = false;

            toolShelfSlider = new SliderBar();
            toolShelfSlider.Editable = true;

            toolShelfMultilineTextbox = new MultiLineTextBox();
            toolShelfMultilineTextbox.Editable = true;

            UIToolShelf.AddItemSlot(new Point(32, 32), ElementType.TEXTBOX);
            UIToolShelf.AddItem(toolShelfTextbox, Point.Zero, ElementType.TEXTBOX);

            UIToolShelf.AddItemSlot(new Point(32, 96), ElementType.LABEL);
            UIToolShelf.AddItem(toolShelfLabel, Point.Zero, ElementType.LABEL);

            UIToolShelf.AddItemSlot(new Point(32, 160), ElementType.BUTTON);
            UIToolShelf.AddItem(toolShelfButton, Point.Zero, ElementType.BUTTON);


            UIToolShelf.AddItemSlot(new Point(32, 224), ElementType.COMBOBOX);
            UIToolShelf.AddItem(toolShelfComboBox, Point.Zero, ElementType.COMBOBOX);

            UIToolShelf.AddItemSlot(new Point(32, 288), ElementType.CHECKBOX);
            UIToolShelf.AddItem(toolShelfCheckbox, Point.Zero, ElementType.CHECKBOX);

            UIToolShelf.AddItemSlot(new Point(32, 352), ElementType.TOGGLE);
            UIToolShelf.AddItem(toolShelfToggle, Point.Zero, ElementType.TOGGLE);

            UIToolShelf.AddItemSlot(new Point(16, 416), ElementType.SLIDER);
            UIToolShelf.AddItem(toolShelfSlider, Point.Zero, ElementType.SLIDER);

            UIToolShelf.AddItemSlot(new Point(16, 480), ElementType.MULTILINE_TEXTBOX);
            UIToolShelf.AddItem(toolShelfMultilineTextbox, Point.Zero, ElementType.MULTILINE_TEXTBOX);

            // Item properties shelf elements
            searchedElements = new List<string>();
            
            itemPropertiesPanel = new Frame("DefaultPropertiesPanelTextureName", ElementDragOption.DRAGGABLE, SpritePriority.NORMAL);
            itemPropertiesPanel.Position = new Point(1590, 100);
         
            objPropColor = new ComboBox("DefaultPropertiesPanelColorComboTextureName", "DefaultPropertiesPanelComboItemTextureName" , "DefaultPropertiesPanelComboItemFootTextureName", SpritePriority.LOW);

            objPropColor.Editable = false;
            objPropColor.DefaultOffset = new Point(0, 24);
            objPropColor.AddAuxiliaryInfo();
          
            //objPropColor.AddClickableDelegate();
            objPropColor.AddName("White", Color.White, Singleton.Font.FONT(FontMgr.FontType.ARIAL));
            objPropColor.AddNewItem("Black", () =>
            {
                objPropColor.Text = "Black";
                objPropColor.AuxilaryColor.Color = Color.Black;

                if (currentObject != null && currentObject.Editable)
                {
                    currentObject.ColorValue = objPropColor.AuxilaryColor;                  
                }
         
                objPropColor.Hide();
            });
            objPropColor.AddNewItem("White", () =>
            {
                objPropColor.Text = "White";
                objPropColor.AuxilaryColor.Color = Color.White;

                if (currentObject != null && currentObject.Editable)
                {
                    currentObject.ColorValue = objPropColor.AuxilaryColor;
                
                }
                        
                objPropColor.Hide();
                
            });
            objPropColor.AddNewItem("Green", () =>
            {
                objPropColor.AuxilaryColor.Color = Color.Green;
                objPropColor.Text = "Green";

                if (currentObject != null && currentObject.Editable)
                {
                    currentObject.ColorValue = objPropColor.AuxilaryColor;                 
                }
         
                objPropColor.Hide();

            });

            objPropTextColor = new ComboBox("DefaultPropertiesPanelColorComboTextureName", "DefaultPropertiesPanelComboItemTextureName", "DefaultPropertiesPanelComboItemFootTextureName", SpritePriority.LOWEST);

            objPropTextColor.Editable = false;
            objPropTextColor.DefaultOffset = new Point(0, 24);
            objPropTextColor.AddAuxiliaryInfo();
           
            //objPropTextColor.AddClickableDelegate();
            objPropTextColor.AddName("White", Color.White, Singleton.Font.FONT(FontMgr.FontType.ARIAL));
            objPropTextColor.AddNewItem("Black", () =>
            {
                objPropTextColor.Text = "Black";
                objPropTextColor.AuxilaryColor.Color = Color.Black;

                if (currentObject != null && currentObject.Editable)
                {
                    currentObject.TextColor = objPropTextColor.AuxilaryColor;
                }

                objPropTextColor.Hide();
            });

            objPropTextColor.AddNewItem("White", () =>
            {
                objPropTextColor.Text = "White";
                objPropTextColor.AuxilaryColor.Color = Color.White;

                if (currentObject != null && currentObject.Editable)
                {
                    currentObject.TextColor = objPropTextColor.AuxilaryColor;

                }

                objPropTextColor.Hide();

            });
            objPropTextColor.AddNewItem("Green", () =>
            {
                objPropTextColor.AuxilaryColor.Color = Color.Green;
                objPropTextColor.Text = "Green";

                if (currentObject != null && currentObject.Editable)
                {
                    currentObject.TextColor = objPropTextColor.AuxilaryColor;
                }

                objPropTextColor.Hide();

            });

            objPropText = new TextBox("DefaultPropertiesPanelTextBoxTextureName", "DefaultPropertiesPanelTextBoxPointerTextureName", TextBoxType.TEXT, SpritePriority.NORMAL);
            objPropText.Editable = false;
            objPropText.FieldWidth = 150;
            objPropName = new Label("");
            objPropName.AddFont(Singleton.Font.FONT(FontMgr.FontType.LUCIDA_CONSOLE));
                 
            objPropTextureName = new TextBox("DefaultPropertiesPanelTextBoxTextureName", "DefaultPropertiesPanelTextBoxPointerTextureName", TextBoxType.TEXT, SpritePriority.HIGHEST);
            objPropTextureName.FieldWidth = 150;

            objPropTextureLoad = new Button("DefaultPropertiesPanelFilePickerButtonTextureName", SpriteLayerOption.NORMAL, SpritePriority.NORMAL);
            objPropTextureLoad.Text = "";

            objPropShowHide = new CheckBox("DefaultPropertiesPanelCheckboxTextureName", SpriteLayerOption.CHECKBOX, SpritePriority.LOW);

            objPropSearch = new TextBox("DefaultPropertiesPanelTextBoxTextureName", "DefaultPropertiesPanelTextBoxPointerTextureName", TextBoxType.TEXT, SpritePriority.HIGHEST);
            objPropSearch.Editable = false;
            objPropSearch.FieldWidth = 320;
            objPropSearch.SampleText = "Search for an item here...";
            objPropSearch.StickSampleText = true;

            objPropLock = new CheckBox("DefaultPropertiesPanelCheckboxTextureName", SpriteLayerOption.CHECKBOX, SpritePriority.LOW);
            objPropLock.Editable = false;


            objPropPositionX = new ModifiableCombinedArrows();
            objPropPositionX.Editable = false;

            objPropPositionY = new ModifiableCombinedArrows();
            objPropPositionY.Editable = false;

            objPropScaleX = new ModifiableCombinedArrows();            
            objPropScaleX.Editable = false;

            objPropScaleY = new ModifiableCombinedArrows();
            objPropScaleY.Editable = false;

            objPropAlpha = new ModifiableCombinedArrows("DefaultProperiesPanelCombinedArrowsLargeTextureName");
            objPropAlpha.Editable = false;            

            objPropItemSelection = new ElementSelection();
            objPropItemSelection.Editable = false;
            objPropItemSelection.Position = Point.Zero;
            objPropEvtOnClickPicker = new TextBoxConfirmPicker();
            Button onClickConfirmButton = new Button("DefaultTextBoxPickerConfirmButtonTextureName", SpriteLayerOption.NORMAL, SpritePriority.LOWEST);
            Button onClickCancelButton = new Button("DefaultTextBoxPickerCancelButtonTextureName", SpriteLayerOption.NORMAL, SpritePriority.LOWEST);
            onClickConfirmButton.Text = "";
            onClickCancelButton.Text = "";
            objPropEvtOnClickPicker.AddButton(onClickCancelButton);
            objPropEvtOnClickPicker.AddButton(onClickConfirmButton);
            
            objPropEvtOnClickPicker.Hide();
            objPropEvtOnClickPicker.Editable = false;
            objPropEvtOnClickPicker.AddMouseClickEvent(() =>
            {
                objPropEvtOnClickPicker.SetSelected(true);                
            },this);
            onClickConfirmButton.AddMouseClickEvent(() =>
            {
                if(currentObject != null)
                {
                    var projectAssembly = GetType().GetTypeInfo().Assembly;
                    Type main = projectAssembly.DefinedTypes.Where(t => t.Name == "Main").FirstOrDefault().AsType();
                    MethodInfo method = main.GetMethod(objPropEvtOnClickPicker.Text);                    
                    
                    if (method != null)
                    {
                        currentObject.MouseEvent += new MouseClickArgs(currentObject, method, this);
                        EventHandler<MouseClickArgs> handler = (EventHandler<MouseClickArgs>)method.CreateDelegate(typeof(EventHandler<MouseClickArgs>), this);                                              
                        currentObject.MouseEvent.OnClick += handler;                        
                    }
                    objPropEvtOnClickPicker.Hide();
                    objPropEvents.Hide();
                }
            }, this);
            onClickCancelButton.AddMouseClickEvent(() =>
            {
                if(currentObject != null)
                {
                    objPropEvtOnClickPicker.Clear();
                    currentObject.MouseEvent.OnClick = null;
                    currentObject.MouseEvent.MouseClickEventArgs = null;
                    objPropEvtOnClickPicker.Hide();
                    objPropEvents.Hide();
                }
               

            }, this);

            objPropEvtOnMouseOverPicker = new TextBoxConfirmPicker();
            Button onMouseOverConfirmButton = new Button("DefaultTextBoxPickerConfirmButtonTextureName", SpriteLayerOption.NORMAL, SpritePriority.LOWEST);
            Button onMouseOverCancelButton = new Button("DefaultTextBoxPickerCancelButtonTextureName", SpriteLayerOption.NORMAL, SpritePriority.LOWEST);
            onMouseOverConfirmButton.Text = "";
            onMouseOverCancelButton.Text = "";
            objPropEvtOnMouseOverPicker.AddButton(onMouseOverCancelButton);
            objPropEvtOnMouseOverPicker.AddButton(onMouseOverConfirmButton);

            objPropEvtOnMouseOverPicker.Hide();
            objPropEvtOnMouseOverPicker.Editable = false;
            objPropEvtOnMouseOverPicker.AddMouseClickEvent(() =>
            {
                objPropEvtOnMouseOverPicker.SetSelected(true);
            }, this);
            onMouseOverConfirmButton.AddMouseClickEvent(() =>
            {
                if (currentObject != null)
                {
                    var projectAssembly = GetType().GetTypeInfo().Assembly;
                    Type main = projectAssembly.DefinedTypes.Where(t => t.Name == "Main").FirstOrDefault().AsType();
                    MethodInfo method = main.GetMethod(objPropEvtOnMouseOverPicker.Text);

                    if (method != null)
                    {
                        currentObject.MouseEvent += new MouseOverArgs(currentObject, method, this);
                        EventHandler<MouseOverArgs> handler = (EventHandler<MouseOverArgs>)method.CreateDelegate(typeof(EventHandler<MouseOverArgs>), this);
                        currentObject.MouseEvent.OnMouseOver += handler;
                    }
                    objPropEvtOnMouseOverPicker.Hide();
                    objPropEvents.Hide();
                }
            }, this);
            onMouseOverCancelButton.AddMouseClickEvent(() =>
            {
                if (currentObject != null)
                {
                    objPropEvtOnMouseOverPicker.Clear();
                    currentObject.MouseEvent.OnMouseOver = null;
                    currentObject.MouseEvent.MouseOverEventArgs = null;
                    objPropEvtOnMouseOverPicker.Hide();
                    objPropEvents.Hide();
                }


            }, this);


            objPropEvtOnMouseOutPicker = new TextBoxConfirmPicker();
            Button onMouseOutConfirmButton = new Button("DefaultTextBoxPickerConfirmButtonTextureName", SpriteLayerOption.NORMAL, SpritePriority.LOWEST);
            Button onMouseOutCancelButton = new Button("DefaultTextBoxPickerCancelButtonTextureName", SpriteLayerOption.NORMAL, SpritePriority.LOWEST);
            onMouseOutConfirmButton.Text = "";
            onMouseOutCancelButton.Text = "";
            objPropEvtOnMouseOutPicker.AddButton(onMouseOutCancelButton);
            objPropEvtOnMouseOutPicker.AddButton(onMouseOutConfirmButton);

            objPropEvtOnMouseOutPicker.Hide();
            objPropEvtOnMouseOutPicker.Editable = false;
            objPropEvtOnMouseOutPicker.AddMouseClickEvent(() =>
            {
                objPropEvtOnMouseOutPicker.SetSelected(true);
            }, this);
            onMouseOutConfirmButton.AddMouseClickEvent(() =>
            {
                if (currentObject != null)
                {
                    var projectAssembly = GetType().GetTypeInfo().Assembly;
                    Type main = projectAssembly.DefinedTypes.Where(t => t.Name == "Main").FirstOrDefault().AsType();
                    MethodInfo method = main.GetMethod(objPropEvtOnMouseOutPicker.Text);

                    if (method != null)
                    {
                        currentObject.MouseEvent += new MouseOutArgs(currentObject, method, this);
                        EventHandler<MouseOutArgs> handler = (EventHandler<MouseOutArgs>)method.CreateDelegate(typeof(EventHandler<MouseOutArgs>), this);
                        currentObject.MouseEvent.OnMouseOut += handler;
                    }
                    objPropEvtOnMouseOutPicker.Hide();
                    objPropEvents.Hide();
                }
            }, this);
            onMouseOutCancelButton.AddMouseClickEvent(() =>
            {
                if (currentObject != null)
                {
                    objPropEvtOnMouseOutPicker.Clear();
                    currentObject.MouseEvent.OnMouseOut = null;
                    currentObject.MouseEvent.MouseOutEventArgs = null;
                    objPropEvtOnMouseOutPicker.Hide();
                    objPropEvents.Hide();
                }


            }, this);

            objPropComboAddItem = new TextBoxConfirmPicker();
            Button addItemButton = new Button("DefaultPropertiesPanelComboAddItemTextureName(1)", SpriteLayerOption.NORMAL, SpritePriority.NORMAL);
            Button remItemButton = new Button("DefaultPropertiesPanelComboRemoveItemTextureName(1)", SpriteLayerOption.NORMAL, SpritePriority.NORMAL);
          
            addItemButton.Text = "";
            remItemButton.Text = "";

            objPropComboAddItem.AddButton(addItemButton, 4);
            objPropComboAddItem.AddButton(remItemButton, 4);

            objPropComboAddItem.AddMouseClickEvent(() =>
            {
                objPropComboAddItem.SetSelected(true);
            }, this);

            addItemButton.AddMouseClickEvent(() =>
            {

                if(currentObject != null && !string.IsNullOrEmpty(objPropComboAddItem.Text))
                {
                    ComboBox cb = currentObject as ComboBox;
                    MessageDialog msg = new MessageDialog("This item already exists.");
                    if (!cb.Contains(objPropComboAddItem.Text))
                    {
                        cb.AddNewItem(objPropComboAddItem.Text, () => { }, true);

                        cb.Hide();
                        cb.Setup();
                        cb.Show();
                        objPropComboAddItem.Text = "";
                    }
                    else
                    {
                        msg.ShowAsync();
                    }
                }
            }, this);

            remItemButton.AddMouseClickEvent(() =>
            {
                if (currentObject != null && !string.IsNullOrEmpty(objPropComboAddItem.Text))
                {                    
                    ComboBox cb = currentObject as ComboBox;
                    if(cb.Contains(objPropComboAddItem.Text))
                    {
                        UIObject item = cb.GetItem(objPropComboAddItem.Text);
                        cb.RemoveItem(item);
                        cb.Hide();
                        cb.Setup();
                        cb.Show();
                        objPropComboAddItem.Text = "";
                    }
                
                }
            }, this);


            objPropEvents = new ComboBox("DefaultPropertiesPanelEventsComboTextureName", "DefaultPropertiesPanelEventsComboItemTextureName", "DefaultPropertiesPanelComboItemFootTextureName", SpritePriority.LOWEST);
            objPropEvents.Editable = false;
            objPropEvents.DefaultOffset = new Point(0, 24);
            objPropEvents.AddName("Mouse Events", Color.White, Singleton.Font.FONT(FontMgr.FontType.GEORGIA));
            objPropEvents.AddMouseClickEvent(() =>
            {
               
                if (currentObject != null)
                {
                    MouseClickArgs clickArgs = currentObject.MouseEvent.MouseClickEventArgs;
                    MouseOverArgs overArgs = currentObject.MouseEvent.MouseOverEventArgs;
                    MouseOutArgs outArgs = currentObject.MouseEvent.MouseOutEventArgs;

                    objPropEvtOnClickPicker.Text = clickArgs != null && clickArgs.ActionMethodName != null? clickArgs.ActionMethodName : "";
                    objPropEvtOnMouseOverPicker.Text = overArgs != null && clickArgs.ActionMethodName != null ? overArgs.ActionMethodName : "";
                    objPropEvtOnMouseOutPicker.Text = outArgs != null && clickArgs.ActionMethodName != null ? outArgs.ActionMethodName : "";
                    
                }
                if(objPropEvents.Closed)
                {
                    objPropEvtOnClickPicker.Hide();
                    objPropEvtOnMouseOverPicker.Hide();
                    objPropEvtOnMouseOutPicker.Hide();
                }
               
            }, this);
            objPropEvents.AddNewItem("OnClick", () =>
            {
                if (currentObject != null)
                {
                    objPropEvtOnClickPicker.Position = new Point(objPropEvents.GetItem(2).Position.X - objPropEvtOnClickPicker.Width, objPropEvents.GetItem(2).Rect.Center.Y - objPropEvtOnClickPicker.Height / 2);
                    objPropEvtOnClickPicker.Show();
                    objPropEvtOnMouseOverPicker.Hide();
                    objPropEvtOnMouseOutPicker.Hide();
                    objPropEvtOnClickPicker.SetSelected(true);
                }
             
                //objPropEvents.Hide();
            });
              
            objPropEvents.AddNewItem("MouseOver", ()=>
            {
                if (currentObject != null)
                {

                    objPropEvtOnMouseOverPicker.Position = new Point(objPropEvents.GetItem(2).Position.X - objPropEvtOnMouseOverPicker.Width, objPropEvents.GetItem(2).Rect.Center.Y - objPropEvtOnMouseOverPicker.Height / 2);
                    objPropEvtOnMouseOverPicker.Show();
                    objPropEvtOnClickPicker.Hide();                    
                    objPropEvtOnMouseOutPicker.Hide();
                    objPropEvtOnMouseOverPicker.SetSelected(true);
                }
            });
            objPropEvents.AddNewItem("MouseOut", ()=>
            {
                if (currentObject != null)
                {

                    objPropEvtOnMouseOutPicker.Position = new Point(objPropEvents.GetItem(2).Position.X - objPropEvtOnMouseOutPicker.Width, objPropEvents.GetItem(2).Rect.Center.Y - objPropEvtOnMouseOutPicker.Height / 2);
                    objPropEvtOnMouseOutPicker.Show();
                    objPropEvtOnClickPicker.Hide();
                    objPropEvtOnMouseOverPicker.Hide();
                    objPropEvtOnMouseOutPicker.SetSelected(true);
                }
            });
            objPropEvents.AddClickableDelegate();
            FileOpenPicker picker = new FileOpenPicker();
            picker.ViewMode = PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".jpg");
            picker.FileTypeFilter.Add(".jpeg");
            picker.FileTypeFilter.Add(".png");

            objPropTextureLoad.AddMouseClickEvent(async () =>
            {
                
                StorageFile file = await picker.PickSingleFileAsync();
                if(file != null)
                {
                    objPropTextureName.Text = file.Name;
                    if(currentObject != null)
                    {
                        objPropTextureName.Text = objPropTextureName.Text.Replace(".jpg", "");
                        objPropTextureName.Text = objPropTextureName.Text.Replace(".png", "");
                        currentObject.UpdateTexture(objPropTextureName.Text);
                    
                    }
                }
                
            }, this);
        

            objPropPositionX.AddOnClickEvents(() =>
            {
                if (currentObject != null)
                {
                    Point position = currentObject.Position;
                    position = new Point(position.X + 4, position.Y);
                    mainWindowFrame.UpdateSlot(currentObject, position);
                }
            },
            () =>
            {
                if (currentObject != null)
                {
                    Point position = currentObject.Position;
                    position = new Point(position.X - 4, position.Y);
                    mainWindowFrame.UpdateSlot(currentObject, position);
                }
            }, this);

            objPropPositionY.AddOnClickEvents(() =>
            {
                if (currentObject != null)
                {
                    Point position = currentObject.Position;
                    position = new Point(position.X , position.Y - 4);
                    mainWindowFrame.UpdateSlot(currentObject, position);
                }
            },
            () =>
            {
                if (currentObject != null)
                {
                    Point position = currentObject.Position;
                    position = new Point(position.X, position.Y +4);
                    mainWindowFrame.UpdateSlot(currentObject, position);
                }
            }, this);

            objPropAlpha.AddOnClickEvents(() =>
            {
                if (currentObject != null)
                {
                    if (currentObject.Alpha < 1.0f)
                    {
                        currentObject.Alpha += 0.25f;
                    }
                }
            },
            () =>
            {
                if (currentObject != null && mainWindowFrame.Contains(currentObject))
                {
                    if (currentObject.Alpha > 0.0f)
                    {
                        currentObject.Alpha -= 0.25f;
                    }
                }
            }, this);         

            objPropText.AddMouseClickEvent(() =>
            {
                objPropText.SetSelected(true);
            }, this);
            objPropText.AddKeyboardEvent(() =>
            {
                if (currentObject != null)
                {
                    currentObject.Text = objPropName.Text == currentObject.Name ? objPropText.Text : currentObject.Text;
                }
            });

            objPropShowHide.AddMouseClickEvent(() =>
            {
                if(currentObject != null)
                {
                    objPropShowHide.SetSelected(!objPropShowHide.Clicked);
                    if (objPropShowHide.Clicked)
                        currentObject.Show();
                    else
                        currentObject.Hide();
                }
            }, this);

            objPropSearch.AddMouseClickEvent(() =>
            {
                objPropSearch.SetSelected(true);

            }, this);
            objPropSearch.AddKeyboardEvent(() => 
            {               
                string text = objPropSearch.Text;
                if (Singleton.Input.KeyReleased(Keys.Back))
                {                  
                    if (!string.IsNullOrEmpty(text))
                    {
                        text = text.Remove(text.Length - 1, 1);
                        currentObject = null;
                        objPropItemSelection.Hide();
                    }
                }
                else
                {                    
                    if (Singleton.Input.KeyReleased(Singleton.Input.CurrentKey))
                    {
                        if (!string.IsNullOrEmpty(text))
                        {

                            if (text.Length > 2)
                            {
                                text = text.Replace("\\", "");
                                string pattern = @".*(?=" + text + ")+";
                                Regex reg = new Regex(pattern, RegexOptions.IgnoreCase);
                                var objectsFound = mainWindowFrame.Slots.Where(t => reg.Match(t.Item.Name).Success).Take(1).ToList();
                                
                                if (objectsFound.Count > 0)
                                {
                                    currentObject = objectsFound[0].Item;
                                    objPropSearch.Text = currentObject.Name;

                                    objPropItemSelection.Position = currentObject.Position;

                                    Point rightBarPosition = new Point(currentObject.Rect.Right, currentObject.Rect.Top);
                                    Point leftBarPosition = new Point(currentObject.Rect.Left, currentObject.Rect.Top);
                                    Point topBarPosition = new Point(currentObject.Rect.Left, currentObject.Rect.Top);
                                    Point bottomBarPosition = new Point(currentObject.Rect.Left, currentObject.Rect.Bottom);

                                    objPropItemSelection.UpdatePosition(rightBarPosition,
                                                                                  leftBarPosition,
                                                                                  topBarPosition,
                                                                                  bottomBarPosition);

                                    objPropItemSelection.UpdateSize(currentObject.Rect.Height,
                                                                              currentObject.Rect.Height,
                                                                               currentObject.Rect.Width,
                                                                               currentObject.Rect.Width);


                                    itemPropertiesPanel.UpdateSlot(objPropItemSelection, currentObject.Position);
                                   
                                    objPropSearch.Text = currentObject.Name;
                                    objPropItemSelection.Show();
                                }                            
                            }
                        }
                    }                  
                }             
            });         

            objPropLock.AddMouseClickEvent(() =>
            {

                currentObject.Editable = !currentObject.Editable;
                //if (!currentObject.Editable)
                //{
                //    currentObject.AddClickableDelegate();
                //}
                //else
                //{
                //    currentObject.AddClickableDelegate();
                //}
               
            }, this);

            objPropScaleX.AddOnClickEvents(() =>
            {
                if (currentObject != null)
                {
                    currentObject.Resize(new Point(1, 0));
                    objPropScaleX.Text = currentObject.Size.X.ToString();
                }
            },
            () =>
            {
                if (currentObject != null)
                {
                    currentObject.Resize(new Point(-1, 0));
                    objPropScaleX.Text = currentObject.Size.X.ToString();
                }
            }, this);

            objPropScaleY.AddOnClickEvents(() =>
            {
                if (currentObject != null)
                {                 
                    currentObject.Resize(new Point(0,1));
                    objPropScaleY.Text = currentObject.Size.Y.ToString();
                }
            },
            () =>
            {                
                if (currentObject != null)
                {            
                    currentObject.Resize(new Point(0,-1));
                    objPropScaleY.Text = currentObject.Size.Y.ToString();
                }
            }, this);

            
            itemPropertiesPanel.AddItemSlot(new Point(191, 212), ElementType.COMBINED_ARROWS, SpritePriority.NORMAL);
            itemPropertiesPanel.AddItem(objPropPositionX, Point.Zero, ElementType.COMBINED_ARROWS);           

            itemPropertiesPanel.AddItemSlot(new Point(266, 212), ElementType.COMBINED_ARROWS, SpritePriority.NORMAL);
            itemPropertiesPanel.AddItem(objPropPositionY, Point.Zero, ElementType.COMBINED_ARROWS);           

            itemPropertiesPanel.AddItemSlot(new Point(175, 368), ElementType.LABEL, SpritePriority.NORMAL);
            itemPropertiesPanel.AddItem(objPropName, Point.Zero, ElementType.LABEL);
        
            itemPropertiesPanel.AddItemSlot(new Point(274, 241), ElementType.BUTTON, SpritePriority.NORMAL);
            itemPropertiesPanel.AddItem(objPropTextureLoad, Point.Zero, ElementType.BUTTON);

            itemPropertiesPanel.AddItemSlot(new Point(169, 240), ElementType.TEXTBOX, SpritePriority.NORMAL);
            itemPropertiesPanel.AddItem(objPropTextureName, Point.Zero, ElementType.TEXTBOX);

            itemPropertiesPanel.AddItemSlot(new Point(171, 271), ElementType.TEXTBOX, SpritePriority.NORMAL);                       
            itemPropertiesPanel.AddItem(objPropText, Point.Zero, ElementType.TEXTBOX);

            itemPropertiesPanel.AddItemSlot(new Point(4, 55), ElementType.TEXTBOX, SpritePriority.NORMAL);
            itemPropertiesPanel.AddItem(objPropSearch, Point.Zero, ElementType.TEXTBOX);

            itemPropertiesPanel.AddItemSlot(new Point(170, 514), ElementType.COMBINED_ARROWS, SpritePriority.NORMAL);
            itemPropertiesPanel.AddItem(objPropAlpha, Point.Zero, ElementType.COMBINED_ARROWS);    

            itemPropertiesPanel.AddItemSlot(new Point(171, 181), ElementType.COMBOBOX, SpritePriority.LOW);
            itemPropertiesPanel.AddItem(objPropColor, Point.Zero, ElementType.COMBOBOX);

            itemPropertiesPanel.AddItemSlot(new Point(172, 395), ElementType.COMBOBOX, SpritePriority.LOWEST);
            itemPropertiesPanel.AddItem(objPropTextColor, Point.Zero, ElementType.COMBOBOX);

            itemPropertiesPanel.AddItemSlot(new Point(176, 488), ElementType.CHECKBOX, SpritePriority.LOW);
            itemPropertiesPanel.AddItem(objPropShowHide, Point.Zero, ElementType.CHECKBOX);

            itemPropertiesPanel.AddItemSlot(new Point(180, 339), ElementType.CHECKBOX, SpritePriority.LOW);
            itemPropertiesPanel.AddItem(objPropLock, Point.Zero, ElementType.CHECKBOX);

            itemPropertiesPanel.AddItemSlot(new Point(193, 455), ElementType.COMBINED_ARROWS, SpritePriority.LOW);
            itemPropertiesPanel.AddItem(objPropScaleX, Point.Zero, ElementType.COMBINED_ARROWS);

            itemPropertiesPanel.AddItemSlot(new Point(266, 455), ElementType.COMBINED_ARROWS, SpritePriority.LOW);
            itemPropertiesPanel.AddItem(objPropScaleY, Point.Zero, ElementType.COMBINED_ARROWS);

            itemPropertiesPanel.AddItemSlot(new Point(0, 0), ElementType.ELEMENT_SELECTION, SpritePriority.LOW);
            itemPropertiesPanel.AddItem(objPropItemSelection, Point.Zero, ElementType.ELEMENT_SELECTION);

            itemPropertiesPanel.AddItemSlot(new Point(171, 118), ElementType.COMBOBOX, SpritePriority.LOWEST);
            itemPropertiesPanel.AddItem(objPropEvents, Point.Zero, ElementType.COMBOBOX);

            itemPropertiesPanel.AddItemSlot(new Point(55, 555), ElementType.TEXT_CONFIRM_PICKER, SpritePriority.NORMAL);
            itemPropertiesPanel.AddItem(objPropComboAddItem, Point.Zero, ElementType.TEXT_CONFIRM_PICKER);
            itemPropertiesPanel.Show();
            UIToolShelf.Show();

            mainFrameCaption = new Label("This is the Actual Window");
            mainFrameCaption.AddFont(Singleton.Font.FONT(FontMgr.FontType.DIALOG_TITLE));
            mainWindowFrame = new Frame("ScreenFrameDefaultTextureName(2)", ElementDragOption.STATIC, SpritePriority.HIGH);
            mainWindowFrame.Show();
            objPropComboAddItem.Hide();
            guiList.Add(objPropEvtOnClickPicker);
            guiList.Add(objPropEvtOnMouseOverPicker);     
            guiList.Add(objPropEvtOnMouseOutPicker);
            guiList.Add(fileMenu);
            guiList.Add(itemPropertiesPanel);
            guiList.Add(mainWindowFrame); 
            guiList.Add(UIToolShelf);            
            
           
         

            base.Initialize();
        }
        
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);       

            UIToolShelf.AddSpriteRenderer(spriteBatch);
            UIToolShelf.AddStringRenderer(spriteBatch);

            mainWindowFrame.AddSpriteRenderer(spriteBatch);
            mainWindowFrame.AddStringRenderer(spriteBatch);

            itemPropertiesPanel.AddSpriteRenderer(spriteBatch);
            itemPropertiesPanel.AddStringRenderer(spriteBatch);

            mainFrameCaption.AddStringRenderer(spriteBatch);

            fileMenu.AddStringRenderer(spriteBatch);
            fileMenu.AddSpriteRenderer(spriteBatch);

            objPropEvtOnClickPicker.AddStringRenderer(spriteBatch);
            objPropEvtOnClickPicker.AddSpriteRenderer(spriteBatch);

            objPropEvtOnMouseOverPicker.AddStringRenderer(spriteBatch);
            objPropEvtOnMouseOverPicker.AddSpriteRenderer(spriteBatch);

            objPropEvtOnMouseOutPicker.AddStringRenderer(spriteBatch);
            objPropEvtOnMouseOutPicker.AddSpriteRenderer(spriteBatch);

            mainBatch = spriteBatch;

            mainWindowFrame.Position = new Point(568, 100);
            mainWindowFrame.Setup();

            Vector2 textSize = mainFrameCaption.TextFont.Font.MeasureString(mainFrameCaption.Text);
            Vector2 textPosition = new Vector2(mainWindowFrame.Rect.Center.X - textSize.X / 2, mainWindowFrame.Top - textSize.Y);
            mainFrameCaption.AddPosition(textPosition);
            

            UIToolShelf.Setup();
            UIToolShelf.Size += new Point(0,256);
            fileMenu.Setup();
            itemPropertiesPanel.Setup();
            objPropEvtOnClickPicker.Position = new Point(objPropEvents.Position.X - objPropEvtOnClickPicker.Width, objPropEvents.Rect.Center.Y - objPropEvtOnClickPicker.Height / 2);
            objPropEvtOnClickPicker.Setup();

            objPropEvtOnMouseOverPicker.Position = new Point(objPropEvents.Position.X - objPropEvtOnMouseOverPicker.Width, objPropEvents.Rect.Center.Y - objPropEvtOnMouseOverPicker.Height / 2);
            objPropEvtOnMouseOverPicker.Setup();

            objPropEvtOnMouseOutPicker.Position = new Point(objPropEvents.Position.X - objPropEvtOnMouseOutPicker.Width, objPropEvents.Rect.Center.Y - objPropEvtOnMouseOutPicker.Height / 2);
            objPropEvtOnMouseOutPicker.Setup();
            fileMenu.Update();

          
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

       
        Container fromContainer;
        protected override void Update(GameTime gameTime)
        {
            matrix = adapter.GetScaleMatrix();
            WndBounds = new Rectangle(adapter.Viewport.X, adapter.Viewport.Y, adapter.VirtualWidth, adapter.VirtualHeight);

            Singleton.Input.Update();     

            if (Singleton.Input.KeyPressed(Keys.Escape))
            {
                Exit();
            }
            MouseGUI.Update();
            // Debug.WriteLine(MouseGUI.Position);

            Parallel.For(0, guiList.Count, (i) =>
             {
                 guiList[i].Update();
                 guiList[i].Update(gameTime);
             });

            MouseGUI.HitObject = null;

            for (int i = 0; i < guiList.Count; i++)
            {
                MouseGUI.HitObject = guiList[i].HitTest(MouseGUI.Position);
                if (MouseGUI.HitObject != null)
                {
                    //Debug.WriteLine(MouseGUI.hitSprite);
                    break;
                }
            }
          
            if (MouseGUI.HitObject != null)
            {               
                if (MouseGUI.LeftWasPressed)
                {
                    MouseGUI.Focus = MouseGUI.HitObject;

                    MouseGUI.DragOffset = (MouseGUI.Focus.Position - MouseGUI.Position);
                    fromContainer = UIToolShelf.Contains(MouseGUI.Focus) ? UIToolShelf : null;
                    UIObject focusObj = MouseGUI.Focus;
                    if (mainWindowFrame.Contains(focusObj) || mainWindowFrame.Contains(focusObj.Position) && focusObj.Editable)
                    {                        
                        currentObject = MouseGUI.Focus;
                     
                    }                  
                }
                else
                {
                    if (MouseGUI.Focus != null)
                    {
                        if (MouseGUI.Focus.Editable && MouseGUI.Focus.DragOption == ElementDragOption.DRAGGABLE)
                        {
                            // TODO: Make sure this is inside the MainWindowFrame bounds
                            MouseGUI.Focus.Position = (MouseGUI.Position + MouseGUI.DragOffset);
                            MouseGUI.Focus.Update();
                            MouseGUI.Focus.Update(gameTime);
                            objPropItemSelection.Hide();

                        }


                        if (MouseGUI.LeftWasReleased)
                        {

                            if (MouseGUI.Focus is UIObject)
                            {
                                UIObject element = MouseGUI.Focus;
                                element.InvokeOnClick();
                            }

                            if (fromContainer != null)
                            {

                                if (!UIToolShelf.Contains(MouseGUI.Position))
                                {
                                    currentObject = UIObjectCreator.CreateElement(MouseGUI.Focus.Type, MouseGUI.Focus.Position, mainWindowFrame);
                                    MouseGUI.Focus.Active = true;

                                }
                            }
                            if (mainWindowFrame.Contains(MouseGUI.Focus))
                            {
                                mainWindowFrame += MouseGUI.Focus;

                            }

                            MouseGUI.ClearFocus();
                        }
                    }
                }
                if (currentObject != null)
                {
                    if (mainWindowFrame.Contains(currentObject))
                    {
                        objPropText.Text = currentObject.Text;
                        if(currentObject is SimpleSprite)
                        {
                            objPropTextureName.Text = (currentObject as SimpleSprite).DefaultSprite.Name;
                        }
                    
                        objPropColor.AuxilaryColor = currentObject.ColorValue;
                        objPropColor.Text = currentObject.ColorValue.Text;

                        objPropTextColor.AuxilaryColor = currentObject.TextColor;
                        objPropTextColor.Text = currentObject.TextColor.Text;

                        objPropName.Text = currentObject.Name;
                        objPropPositionX.Text = (currentObject.Position.X - mainWindowFrame.X).ToString();
                        objPropPositionY.Text = (currentObject.Position.Y - mainWindowFrame.Y).ToString();
                        objPropAlpha.Text = currentObject.Alpha.ToString();

                        objPropShowHide.SetSelected(currentObject.Active);
                        objPropLock.SetSelected(!currentObject.Editable);

                        objPropScaleX.Text = currentObject.Size.X.ToString();
                        objPropScaleY.Text = currentObject.Size.Y.ToString();                  
                    }
                    if (currentObject is ComboBox)
                    {
                        objPropComboAddItem.Show();
                    }
                    else
                    {
                        objPropComboAddItem.Hide();

                    }
                }
            }
            //else if (MouseGUI.RightWasPressed)
            //{

            //}


            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // TODO: Add your drawing code here           

            GraphicsDevice.Clear(Color.Gray);

            spriteBatch.Begin();
                     
            for (int i = guiList.Count -1; i >= 0; i--)
            {
                guiList[i].Draw();
            }
            spriteBatch.End();


            base.Draw(gameTime);
        }
    }
}
